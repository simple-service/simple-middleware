package com.cms701.simulator.casout.util;

import java.util.HashMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil {

	private static final Gson gson = new GsonBuilder()
			.disableHtmlEscaping()
			.setDateFormat("dd-MM-yyyy HH:mm:ss")
			.create();
	
	public static <T> T fromJson (Class<T> T, String strJson) {
		return gson.fromJson(strJson, T);
    }
	
	public static <T> String toJson (Class<T> T,  T strJson) {
		return gson.toJson(strJson, T);
    }
	
	public static <T> String toJson (T obj) {
		return gson.toJson(obj, obj.getClass());
    }
	
	@SuppressWarnings("unchecked")
	public static <T> HashMap<String, String> toHashmap (T obj) {
		return gson.fromJson(toJson(obj), HashMap.class);
    }
}