package com.cms701.simulator.casout.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms701.simulator.casout.PrefixConf;
import com.cms701.simulator.casout.model.BCAFundTransfer;
import com.cms701.simulator.casout.model.BCASuccessMessage;
import com.cms701.simulator.casout.util.JsonUtil;

@RestController
public class BCAController {
	
	@Autowired private PrefixConf conf;
	
	@PostMapping(value = "fund/transfer")
	public String fundTransfer(@RequestBody String strJson) {
		BCAFundTransfer req = JsonUtil.fromJson(BCAFundTransfer.class, strJson);
		
		BCASuccessMessage res = new BCASuccessMessage();
		if (conf.isFailed(req.getBeneficiaryAccountNumber())) {
			res.setError();
		} else {
			res.setReferenceID(req.getReferenceID());
			res.setStatus("Success");
			res.setTransactionDate(req.getTransactionDate());
			res.setTransactionID(req.getTransactionID());
		}
		
		return JsonUtil.toJson(res);
	}
	
	@PostMapping(value = "fund/transfer/domestic")
	public String domesticFundTransfer(@RequestBody String strJson) {
		BCAFundTransfer req = JsonUtil.fromJson(BCAFundTransfer.class, strJson);
		
		
		BCASuccessMessage res = new BCASuccessMessage();
		if (conf.isFailed(req.getBeneficiaryAccountNumber())) {
			res.setError();
		} else {
			res.setReferenceID(req.getReferenceID());
			res.setStatus("Success");
			res.setTransactionDate(req.getTransactionDate());
			res.setTransactionID(req.getTransactionID());
			res.setPPUNumber("SIMULATOR-" + Math.random());
		}
		
		return JsonUtil.toJson(res);
	}
	
}
