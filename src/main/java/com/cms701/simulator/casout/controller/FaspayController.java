package com.cms701.simulator.casout.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cms701.simulator.casout.PrefixConf;
import com.cms701.simulator.casout.model.FaspayResponse;
import com.cms701.simulator.casout.model.FaspayStatusTransfer;
import com.cms701.simulator.casout.model.FaspayTransferRequest;
import com.cms701.simulator.casout.model.FaspayTransferResponse;
import com.cms701.simulator.casout.util.JsonUtilRev;

@RestController
public class FaspayController {

	@Autowired private PrefixConf conf;
	
	@PostMapping(value = "faspay/transfer")
	public String fundTransfer(@RequestBody String strJson) {
		FaspayTransferRequest req = JsonUtilRev.fromJson(FaspayTransferRequest.class, strJson);
		
		
		FaspayResponse<FaspayTransferResponse> response = new FaspayResponse<FaspayTransferResponse>();
		if (conf.isFailed(req.getDest_account())) {
			response.setCode("99");
			response.setMessage("Transfer gagal dari faspay");
		
		} else {
			response.setCode("00");
			response.setMessage("Success");
			response.setData(new FaspayTransferResponse(
				req.getDest_va(), 
				req.getDest_account(), 
				req.getDest_account_name(), 
				req.getDest_bank(), 
				req.getDest_bank_name(), 
				req.getTrx_id(), 
				req.getTrx_date(), 
				req.getTrx_amount(), 
				req.getTrx_desc(), 
				new SimpleDateFormat("HHmmss").format(new Date()), 
				UUID.randomUUID().toString(), 
				"1")
			);
		}
		
		return JsonUtilRev.toJson(response);
	}
	
	@GetMapping(value = "faspay/transfer/status/{trxDigi}/{trxFaspay}")
	public String fundTransfer(@PathVariable String trxDigi, @PathVariable String trxFaspay) {
		FaspayResponse<FaspayStatusTransfer> response = new FaspayResponse<FaspayStatusTransfer>();
		
		String status = conf.isStatusTransferSuccess() ? "2" : "9";
		FaspayStatusTransfer mStatus = new FaspayStatusTransfer(trxFaspay, UUID.randomUUID().toString(), null, status);
		
		response.setCode("00");
		response.setMessage("Success");
		response.setData(mStatus);
		
		return JsonUtilRev.toJson(response);
	}
}
