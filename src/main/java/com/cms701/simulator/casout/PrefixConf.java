package com.cms701.simulator.casout;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("application.properties")
public class PrefixConf {
	@Value("${trx.tranfer.success}")
	public boolean status_transfer;
	@Value("${trx.prefix.success}")
	public String success;
	@Value("${trx.prefix.failed}")
	public String failed;	
	@Value("${trx.prefix.pending}")
	public String pending;
	
	

	public boolean isSuccess(String account) {
		return account.startsWith(success);
	}
	
	public boolean isPending(String account) {
		return account.startsWith(pending);
	}
	
	public boolean isFailed(String account) {
		return account.startsWith(failed);
	}
	
	public boolean isStatusTransferSuccess() {
		return status_transfer;
	}
	
}
