package com.cms701.simulator.casout.model;

public class BCAFundTransfer {
	private String CorporateID = "";			// Di handle interface
	private String CurrencyCode = "";			// Di handle interface
	private String SourceAccountNumber;			// Rekening asal
	private String TransactionID;				// Unique Trx ID
	private String TransactionDate;				// yyyy-MM-dd
	private String ReferenceID;					// Prefix Refference ID
	private String Amount;						// Nominal
	private String BeneficiaryAccountNumber;	// Destination account
	private String Remark1;						// For reciever
	private String Remark2;						// For reviever
	
	public String getCorporateID() {
		return CorporateID;
	}
	public void setCorporateID(String corporateID) {
		CorporateID = corporateID;
	}
	public String getCurrencyCode() {
		return CurrencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		CurrencyCode = currencyCode;
	}
	public String getSourceAccountNumber() {
		return SourceAccountNumber;
	}
	public void setSourceAccountNumber(String sourceAccountNumber) {
		SourceAccountNumber = sourceAccountNumber;
	}
	public String getTransactionDate() {
		return TransactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}
	public String getReferenceID() {
		return ReferenceID;
	}
	public void setReferenceID(String referenceID) {
		ReferenceID = referenceID;
	}
	public String getAmount() {
		return Amount;
	}
	public void setAmount(String amount) {
		Amount = amount;
	}
	public String getBeneficiaryAccountNumber() {
		return BeneficiaryAccountNumber;
	}
	public void setBeneficiaryAccountNumber(String beneficiaryAccountNumber) {
		BeneficiaryAccountNumber = beneficiaryAccountNumber;
	}
	public String getRemark1() {
		return Remark1;
	}
	public void setRemark1(String remark1) {
		Remark1 = remark1;
	}
	public String getRemark2() {
		return Remark2;
	}
	public void setRemark2(String remark2) {
		Remark2 = remark2;
	}
	public String getTransactionID() {
		return TransactionID;
	}
	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}
	public BCAFundTransfer(String corporateID, String currencyCode, String sourceAccountNumber, String transactionID,
			String transactionDate, String referenceID, String amount, String beneficiaryAccountNumber, String remark1,
			String remark2) {
		super();
		CorporateID = corporateID;
		CurrencyCode = currencyCode;
		SourceAccountNumber = sourceAccountNumber;
		TransactionID = transactionID;
		TransactionDate = transactionDate;
		ReferenceID = referenceID;
		Amount = amount;
		BeneficiaryAccountNumber = beneficiaryAccountNumber;
		Remark1 = remark1;
		Remark2 = remark2;
	}
	public BCAFundTransfer() {
		super();
		// TODO Auto-generated constructor stub
	}	
	
}
