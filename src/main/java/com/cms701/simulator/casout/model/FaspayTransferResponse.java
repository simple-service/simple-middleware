package com.cms701.simulator.casout.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;

public class FaspayTransferResponse extends FaspayTransferRequest {
	public FaspayTransferResponse(String dest_va, String dest_account, String dest_account_name, String dest_bank,
			String dest_bank_name, String trx_id, Date trx_date, BigDecimal trx_amount, String trx_desc,
			String faspay_id, String faspay_reff, String status) {
		super(dest_va, dest_account, dest_account_name, dest_bank, dest_bank_name, trx_id, trx_date, trx_amount,
				trx_desc);
		this.faspay_id = faspay_id;
		this.faspay_reff = faspay_reff;
		this.status = status;
	}
	private String faspay_id;
	private String faspay_reff;
	private String status;
	
	public boolean isSuccess() {
		return this.status.equals("2");
	}
	public boolean isPending() {
		return this.status.equals("1");
	}
	public boolean isFailed() {
		return Arrays.asList("1", "2").contains(status);
	}
	
	public FaspayStatusTransfer getStatus() {
		return new FaspayStatusTransfer(faspay_id, faspay_reff, getDest_va(), status);
	}
	public String getFaspay_id() {
		return faspay_id;
	}
	public void setFaspay_id(String faspay_id) {
		this.faspay_id = faspay_id;
	}
	public String getFaspay_reff() {
		return faspay_reff;
	}
	public void setFaspay_reff(String faspay_reff) {
		this.faspay_reff = faspay_reff;
	}
	public void setStatus(String status) {
		this.status = status;
	}
		
}