package com.cms701.simulator.casout.model;

import java.math.BigDecimal;
import java.util.Date;

public class FaspayTransferRequest {
	public FaspayTransferRequest(String dest_va, String dest_account, String dest_account_name, String dest_bank,
			String dest_bank_name, String trx_id, Date trx_date, BigDecimal trx_amount, String trx_desc) {
		super();
		this.dest_va = dest_va;
		this.dest_account = dest_account;
		this.dest_account_name = dest_account_name;
		this.dest_bank = dest_bank;
		this.dest_bank_name = dest_bank_name;
		this.trx_id = trx_id;
		this.trx_date = trx_date;
		this.trx_amount = trx_amount;
		this.trx_desc = trx_desc;
	}
	private String dest_va;
	private String dest_account;
	private String dest_account_name;
	private String dest_bank;
	private String dest_bank_name;
	private String trx_id;
	private Date trx_date;
	private BigDecimal trx_amount;
	private String trx_desc;
	public String getDest_va() {
		return dest_va;
	}
	public void setDest_va(String dest_va) {
		this.dest_va = dest_va;
	}
	public String getDest_account() {
		return dest_account;
	}
	public void setDest_account(String dest_account) {
		this.dest_account = dest_account;
	}
	public String getDest_account_name() {
		return dest_account_name;
	}
	public void setDest_account_name(String dest_account_name) {
		this.dest_account_name = dest_account_name;
	}
	public String getDest_bank() {
		return dest_bank;
	}
	public void setDest_bank(String dest_bank) {
		this.dest_bank = dest_bank;
	}
	public String getDest_bank_name() {
		return dest_bank_name;
	}
	public void setDest_bank_name(String dest_bank_name) {
		this.dest_bank_name = dest_bank_name;
	}
	public String getTrx_id() {
		return trx_id;
	}
	public void setTrx_id(String trx_id) {
		this.trx_id = trx_id;
	}
	public Date getTrx_date() {
		return trx_date;
	}
	public void setTrx_date(Date trx_date) {
		this.trx_date = trx_date;
	}
	public BigDecimal getTrx_amount() {
		return trx_amount;
	}
	public void setTrx_amount(BigDecimal trx_amount) {
		this.trx_amount = trx_amount;
	}
	public String getTrx_desc() {
		return trx_desc;
	}
	public void setTrx_desc(String trx_desc) {
		this.trx_desc = trx_desc;
	}
}