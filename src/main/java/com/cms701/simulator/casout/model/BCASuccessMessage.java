package com.cms701.simulator.casout.model;

public class BCASuccessMessage extends BCAErrorResponse {
	private String TransactionID;
	private String TransactionDate;
	private String ReferenceID;
	private String Status;
	
	private String PPUNumber;
	
	public String getTransactionDate() {
		return TransactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}
	public String getReferenceID() {
		return ReferenceID;
	}
	public void setReferenceID(String referenceID) {
		ReferenceID = referenceID;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public BCASuccessMessage(String transactionID, String transactionDate, String referenceID, String status) {
		super();
		TransactionID = transactionID;
		TransactionDate = transactionDate;
		ReferenceID = referenceID;
		Status = status;
	}
	public BCASuccessMessage() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getTransactionID() {
		return TransactionID;
	}
	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}
	public String getPPUNumber() {
		return PPUNumber;
	}
	public void setPPUNumber(String pPUNumber) {
		PPUNumber = pPUNumber;
	}
}
