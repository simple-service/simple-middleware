package com.cms701.simulator.casout.model;

import java.util.Arrays;

public class FaspayStatusTransfer {
	private String faspay_id = "-";
	private String faspay_reff = "-";
	private String va_number = "-";
	private String status;
	
	public FaspayStatusTransfer(String faspay_id, String faspay_reff, String va_number, String status) {
		super();
		this.faspay_id = faspay_id;
		this.faspay_reff = faspay_reff;
		this.va_number = va_number;
		this.status = status;
	}
	public FaspayStatusTransfer() {
		super();
		// TODO Auto-generated constructor stub
	}
	public boolean isSuccess() {
		return this.status.equals("2");
	}
	public boolean isPending() {
		return this.status.equals("1");
	}
	public boolean isFailed() {
		return Arrays.asList("1", "2").contains(status);
	}
	
	public FaspayStatusTransfer getStatus() {
		return this;
	}
	public String getFaspay_id() {
		return faspay_id;
	}
	public void setFaspay_id(String faspay_id) {
		this.faspay_id = faspay_id;
	}
	public String getFaspay_reff() {
		return faspay_reff;
	}
	public void setFaspay_reff(String faspay_reff) {
		this.faspay_reff = faspay_reff;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVa_number() {
		return va_number;
	}
	public void setVa_number(String va_number) {
		this.va_number = va_number;
	}
	
}