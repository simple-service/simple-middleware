package com.cms701.simulator.casout.model;

public class BCAErrorResponse {
	public String ErrorCode;
	public Message ErrorMessage;
	
	public static class Message {
		public String Indonesian;
		public String English;
		public Message(String indonesian, String english) {
			super();
			Indonesian = indonesian;
			English = english;
		}
	}
	
	public BCAErrorResponse setError() {
		setErrorCode("ESB-82-019");
		setErrorMessage(new Message("Transfer digagalkan, hahahaaaaaa....", "Transfer rejected !"));
		return this;
	}


	public String getErrorCode() {
		return ErrorCode;
	}


	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}


	public Message getErrorMessage() {
		return ErrorMessage;
	}


	public void setErrorMessage(Message errorMessage) {
		ErrorMessage = errorMessage;
	}
}
