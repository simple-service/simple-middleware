package com.cms701.simulator.casout.model;

public class BCADomesticFundTransfer extends BCAFundTransfer {
	private String BeneficiaryBankCode;		// Dest. Swift Code
    private String BeneficiaryName;			// Dest. Account Name
    private String TransferType = "LLG";
    private String BeneficiaryCustType = "1";
    private String BeneficiaryCustResidence = "1";
    
	public BCADomesticFundTransfer(String CorporateID, String CurrencyCode, String SourceAccountNumber,
			String TransactionID, String TransactionDate, String ReferenceID, String Amount,
			String BeneficiaryAccountNumber, String Remark1, String Remark2, String BeneficiaryBankCode, String BeneficiaryName) {
		
		super(CorporateID, CurrencyCode, SourceAccountNumber, TransactionID, TransactionDate, ReferenceID, Amount, BeneficiaryAccountNumber, Remark1, Remark2);
		this.BeneficiaryBankCode = BeneficiaryBankCode;
		this.BeneficiaryName = BeneficiaryName;
	}

	public String getBeneficiaryBankCode() {
		return BeneficiaryBankCode;
	}

	public void setBeneficiaryBankCode(String beneficiaryBankCode) {
		BeneficiaryBankCode = beneficiaryBankCode;
	}

	public String getBeneficiaryName() {
		return BeneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		BeneficiaryName = beneficiaryName;
	}

	public String getBeneficiaryCustType() {
		return BeneficiaryCustType;
	}

	public void setBeneficiaryCustType(String beneficiaryCustType) {
		BeneficiaryCustType = beneficiaryCustType;
	}

	public String getBeneficiaryCustResidence() {
		return BeneficiaryCustResidence;
	}

	public void setBeneficiaryCustResidence(String beneficiaryCustResidence) {
		BeneficiaryCustResidence = beneficiaryCustResidence;
	}

	public String getTransferType() {
		return TransferType;
	}

	public void setTransferType(String transferType) {
		TransferType = transferType;
	}

	public BCADomesticFundTransfer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BCADomesticFundTransfer(String corporateID, String currencyCode, String sourceAccountNumber,
			String transactionID, String transactionDate, String referenceID, String amount,
			String beneficiaryAccountNumber, String remark1, String remark2) {
		super(corporateID, currencyCode, sourceAccountNumber, transactionID, transactionDate, referenceID, amount,
				beneficiaryAccountNumber, remark1, remark2);
		// TODO Auto-generated constructor stub
	}

	public BCADomesticFundTransfer(String corporateID, String currencyCode, String sourceAccountNumber,
			String transactionID, String transactionDate, String referenceID, String amount,
			String beneficiaryAccountNumber, String remark1, String remark2, String beneficiaryBankCode,
			String beneficiaryName, String transferType, String beneficiaryCustType, String beneficiaryCustResidence) {
		super(corporateID, currencyCode, sourceAccountNumber, transactionID, transactionDate, referenceID, amount,
				beneficiaryAccountNumber, remark1, remark2);
		BeneficiaryBankCode = beneficiaryBankCode;
		BeneficiaryName = beneficiaryName;
		TransferType = transferType;
		BeneficiaryCustType = beneficiaryCustType;
		BeneficiaryCustResidence = beneficiaryCustResidence;
	}
}